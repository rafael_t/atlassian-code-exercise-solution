# REST Node.js | Express API Example

A solution to https://bitbucket.org/dcor_atlassian/atlassian-code-exercise-sr-full-stack-dev-martech/src/master/

## Architecture

This REST API was implemented using Node.js/Express/mySQL, and Docker. Likewise, Docker Compose was leveraged to facilitate local running.

## Running the App

### System Requirements

- Docker
- Docker Compose
- __NOTE__: Make sure both ports `3306` and `8080` are available

### To run the app locally

You need Node (preferrably v8.14.1) + npm. Then on a Terminal window, run:

```bash
npm start
```

This will launch a `docker-compose` environment with `Node` and `mySQL` configured for the app. Once it finishes launching, you should see this message:

```bash
api_server    | App listening on http://localhost:8080
```

At this point, you can exercise the API via http://localhost:8080

As of now, these are the endpoints supported:

```txt
GET: http://localhost:8080/api/health
GET/POST/PUT: http://localhost:8080/api/contacts
GET/POST/PUT: http://localhost:8080/api/accounts
GET: http://localhost:8080/api/accounts/{accountId}/contacts
```

### To run the unit tests

On a separate Terminal window, run:

```bash
npm test
```

This will run the tests in `watch` mode. If you instead, want to run the tests only once, run:

```bash
npm run test:ci
```

## Deployment Instructions

All the config-level values are stored in the `/config` dir.

- The app-level configs are managed by [`config npm`](https://www.npmjs.com/package/config). Hence, they should be stored in a corresponding `[NODE_ENV].json` file, or else `config/default.json` is used.
- The DB-level configs are managed by [`Sequelize`](https://sequelize.org/). Hence, they're stored in `config/database.json`.

For example, if a new `NODE_ENV=production` is to be stood up, you'll need to:

1. Create a `config/production.json` file, using the `config/default.json` template.
2. Add a new `production` node to the `database.json` file to configure the DB.

Note that secret-level concerns should be taken into account for non-dev deployments.

## TODO

- [ ] __Create Open API docs__: This will help document the API and enable easier testing (via [Postman](https://www.postman.com/) for example).
- [ ] __Create a dedicated local test DB to run integration tests__: Right now when integration tests are run locally, they're run against the local dev DB, which causes seed data to be wiped out during test runs.
- [ ] __Leverage Sequelize seeders__: To facilitate local development.
- [ ] __Create test helpers__: For test-logic reusability (e.g., test DB resetting).
- [ ] __Compile solution for production__: Remove dev-level artifacts for production.
