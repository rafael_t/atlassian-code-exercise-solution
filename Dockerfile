FROM node:8.14.1
WORKDIR /var/app
COPY package*.json ./
RUN npm ci --no-optional
COPY . ./
EXPOSE 8080
CMD npm run serve
