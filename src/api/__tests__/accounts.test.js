import request from 'supertest';
import faker from 'faker';

import db from 'db';
import app from 'app';

describe('/api/accounts', () => {
    beforeEach(async () => await db.Account.destroy({ where: {} }));

    describe('GET', () => {
        const account1 = mockAccount();
        const account2 = mockAccount();

        beforeEach(async () => await db.Account.bulkCreate([account1, account2]));

        describe('when no ID is provided', () => {
            it('should get all accounts', (done) => {
                request(app)
                    .get('/api/accounts')
                    .expect(200)
                    .expect('Content-Type', /application\/json/)
                    .then(({ body }) => {
                        const actual = new Set(body);
                        const expected = new Set([account1, account2]);
                        expect(actual).toEqual(expected);
                        done();
                    });
            });
        });
    });

    describe('POST', () => {
        it('should create an account', (done) => {
            const account = {
                companyName: faker.name.firstName(),
                addressLine1: faker.address.streetName(),
                addressLine2: faker.address.secondaryAddress(),
                city: faker.address.city(),
                state: faker.address.state(),
                postalCode: faker.address.zipCode(),
                country: faker.address.country(),
            };

            request(app)
                .post('/api/accounts')
                .send(account)
                .expect(201)
                .then(async () => {
                    const persisted = await db.Account.findOne({
                        options: {
                            where: { ...account },
                        },
                    });
                    expect(persisted).not.toBeUndefined();
                    done();
                });
        });

        describe('if companyName is not provided', () => {
            const account = {};

            it('should return a 400', (done) => {
                request(app)
                    .post('/api/accounts')
                    .send(account)
                    .expect(400)
                    .end(done);
            });

            it('should not create the account', (done) => {
                request(app)
                    .post('/api/accounts')
                    .send(account)
                    .then(async () => {
                        const persisted = await db.Account.findAll();
                        expect(persisted).toHaveLength(0);
                        done();
                    });
            });

            it('should return a proper error message', (done) => {
                request(app)
                    .post('/api/accounts')
                    .send(account)
                    .then(({ body }) => {
                        expect(body).toMatchObject({ error: 'Missing companyName' });
                        done();
                    });
            });
        });
    });

    describe('PUT', () => {
        const account = mockAccount();

        beforeEach(async () => await db.Account.create(account));

        it('should update an account', (done) => {
            const newName = `${ account.name }foobar`;
            request(app)
                .put(`/api/accounts/${ account.id }`)
                .send({ name: newName })
                .expect(200)
                .then(async () => {
                    const persisted = await db.Account.findOne({
                        options: {
                            where: { name: newName },
                        },
                    });
                    expect(persisted).not.toBeUndefined();
                    done();
                });
        });

        describe('if the id is invalid', () => {
            it('should return a 400', (done) => {
                request(app)
                    .put('/api/accounts/123')
                    .send(account)
                    .expect(400)
                    .end(done);
            });

            it('should return a proper error message', (done) => {
                request(app)
                    .put('/api/accounts/123')
                    .send(account)
                    .then(({ body }) => {
                        expect(body).toMatchObject({ error: 'Invalid id' });
                        done();
                    });
            });
        });
    });
});

describe('/api/accounts/:id', () => {
    const account = mockAccount();

    beforeEach(async () => await db.Account.destroy({ where: {} }));

    beforeEach(async () => await db.Account.create(account));

    it('should get the account with the provided ID', (done) => {
        request(app)
            .get(`/api/accounts/${ account.id }`)
            .expect(200)
            .expect('Content-Type', /application\/json/)
            .then(({ body }) => {
                expect(body).toEqual(account);
                done();
            });
    });

    describe('if there is no account matching the provided ID', () => {
        it('should return a 400', (done) => {
            request(app)
                .get('/api/accounts/123')
                .expect(400)
                .end(done);
        });

        it('should return a proper error message', (done) => {
            request(app)
                .get('/api/accounts/123')
                .then(({ body }) => {
                    expect(body).toMatchObject({ error: 'Wrong ID' });
                    done();
                });
        });
    });
});

describe('/api/accounts/:id/contacts', () => {
    const accountWithContacts = mockAccount();
    const accountWithoutContacts = mockAccount();
    const contact1 = mockContact(accountWithContacts.id);
    const contact2 = mockContact(accountWithContacts.id);

    beforeEach(async () => {
        await db.Account.destroy({ where: {} });
        await db.Contact.destroy({ where: {} });
    });

    beforeEach(async () => {
        await db.Account.bulkCreate([accountWithContacts, accountWithoutContacts]);
        await db.Contact.bulkCreate([contact1, contact2]);
    });

    it('should fetch the contacts associated with the provided account ID', (done) => {
        request(app)
            .get(`/api/accounts/${ accountWithContacts.id }/contacts`)
            .expect(200)
            .expect('Content-Type', /application\/json/)
            .then(({ body }) => {
                const actual = new Set(body);
                const expected = new Set([contact1, contact2]);
                expect(actual).toEqual(expected);
                done();
            });
    });

    describe('if there is no account matching the provided account ID', () => {
        it('should return a 400', (done) => {
            request(app)
                .get('/api/accounts/123/contacts')
                .expect(400)
                .end(done);
        });

        it('should return a proper error message', (done) => {
            request(app)
                .get('/api/accounts/123/contacts')
                .then(({ body }) => {
                    expect(body).toMatchObject({ error: 'Wrong ID' });
                    done();
                });
        });
    });

    describe('if there are no contacts associated with the provided account ID', () => {
        it('should return an empty array', (done) => {
            request(app)
                .get(`/api/accounts/${ accountWithoutContacts.id }/contacts`)
                .expect(200)
                .expect('Content-Type', /application\/json/)
                .then(({ body }) => {
                    expect(body).toEqual([]);
                    done();
                });
        });
    });
});

function mockAccount() {
    return {
        id: faker.random.uuid(),
        companyName: faker.company.companyName(),
        addressLine1: faker.address.streetName(),
        addressLine2: faker.address.secondaryAddress(),
        city: faker.address.city(),
        state: faker.address.state(),
        postalCode: faker.address.zipCode(),
        country: faker.address.country(),
    };
};

function mockContact(accountId) {
    return {
        id: faker.random.uuid(),
        accountId,
        name: faker.name.firstName(),
        email: faker.internet.email(),
        addressLine1: faker.address.streetName(),
        addressLine2: faker.address.secondaryAddress(),
        city: faker.address.city(),
        state: faker.address.state(),
        postalCode: faker.address.zipCode(),
        country: faker.address.country(),
    };
};
