import request from 'supertest';
import faker from 'faker';

import db from 'db';
import app from 'app';

describe('/api/contacts', () => {
    beforeEach(async () => {
        await db.Contact.destroy({ where: {} });
        await db.Account.destroy({ where: {} });
    });

    describe('GET', () => {
        let contact1, contact2;

        beforeEach(async () => {
            contact1 = mockContact();
            contact2 = mockContact();
            await db.Contact.bulkCreate([contact1, contact2]);
        });

        describe('when no ID is provided', () => {
            it('should get all contacts', (done) => {
                request(app)
                    .get('/api/contacts')
                    .expect(200)
                    .expect('Content-Type', /application\/json/)
                    .then(({ body }) => {
                        const actual = new Set(body);
                        const expected = new Set([contact1, contact2]);
                        expect(actual).toEqual(expected);
                        done();
                    });
            });
        });
    });

    describe('POST', () => {
        it('should create a contact', (done) => {
            const contact = {
                name: faker.name.firstName(),
                email: faker.internet.email(),
                addressLine1: faker.address.streetName(),
                addressLine2: faker.address.secondaryAddress(),
                city: faker.address.city(),
                state: faker.address.state(),
                postalCode: faker.address.zipCode(),
                country: faker.address.country(),
            };

            request(app)
                .post('/api/contacts')
                .send(contact)
                .expect(201)
                .then(async () => {
                    const persisted = await db.Contact.findOne({
                        options: {
                            where: { ...contact },
                        },
                    });
                    expect(persisted).not.toBeUndefined();
                    done();
                });
        });

        describe('if email is not provided', () => {
            const contact = {};

            it('should return a 400', (done) => {
                request(app)
                    .post('/api/contacts')
                    .send(contact)
                    .expect(400)
                    .end(done);
            });

            it('should not create the contact', (done) => {
                request(app)
                    .post('/api/contacts')
                    .send(contact)
                    .then(async () => {
                        const persisted = await db.Contact.findAll();
                        expect(persisted).toHaveLength(0);
                        done();
                    });
            });

            it('should return a proper error message', (done) => {
                request(app)
                    .post('/api/contacts')
                    .send(contact)
                    .then(({ body }) => {
                        expect(body).toMatchObject({ error: 'Missing email' });
                        done();
                    });
            });
        });
    });

    describe('PUT', () => {
        let contact;

        beforeEach(async () => {
            contact = mockContact();
            await db.Contact.create(contact);
        });

        it('should update a contact', (done) => {
            const newName = `${ contact.name }foobar`;
            request(app)
                .put(`/api/contacts/${ contact.id }`)
                .send({ name: newName })
                .expect(200)
                .then(async () => {
                    const persisted = await db.Contact.findOne({
                        options: {
                            where: { name: newName },
                        },
                    });
                    expect(persisted).not.toBeUndefined();
                    done();
                });
        });

        describe('if the id is invalid', () => {
            it('should return a 400', (done) => {
                request(app)
                    .put('/api/contacts/123')
                    .send(contact)
                    .expect(400)
                    .end(done);
            });

            it('should return a proper error message', (done) => {
                request(app)
                    .put('/api/contacts/123')
                    .send(contact)
                    .then(({ body }) => {
                        expect(body).toMatchObject({ error: 'Invalid id' });
                        done();
                    });
            });
        });

        describe('when accountId is provided', () => {
            let account;

            beforeEach(async () => {
                account = mockAccount();
                await db.Account.create(account);
            });

            it('should associate a contact to the account', (done) => {
                request(app)
                    .put(`/api/contacts/${ contact.id }`)
                    .send({ accountId: account.id })
                    .expect(200)
                    .then(async () => {
                        const persisted = await db.Contact.findByPk(contact.id);
                        expect(persisted.accountId).toEqual(account.id);
                        done();
                    });
            });

            describe('if accountId is invalid', () => {
                it('should return a 400', (done) => {
                    request(app)
                        .put(`/api/contacts/${ contact.id }`)
                        .send({ accountId: '123' })
                        .expect(400)
                        .end(done);
                });

                it('should not update contact.accountId', (done) => {
                    request(app)
                        .put(`/api/contacts/${ contact.id }`)
                        .send({ accountId: '123' })
                        .then(async () => {
                            const persisted = await db.Contact.findByPk(contact.id);
                            expect(persisted.accountId).toBeNull();
                            done();
                        });
                });

                it('should return a proper error message', (done) => {
                    request(app)
                        .put(`/api/contacts/${ contact.id }`)
                        .send({ accountId: '123' })
                        .then(({ body }) => {
                            expect(body).toMatchObject({ error: 'Invalid accountId' });
                            done();
                        });
                });
            });
        });
    });
});

describe('/api/contacts/:id', () => {
    const contact = mockContact();

    beforeEach(async () => {
        await db.Contact.destroy({ where: {} });
        await db.Account.destroy({ where: {} });
    });

    beforeEach(async () => await db.Contact.create(contact));

    it('should get the contact with the provided ID', (done) => {
        request(app)
            .get(`/api/contacts/${ contact.id }`)
            .expect(200)
            .expect('Content-Type', /application\/json/)
            .then(({ body }) => {
                expect(body).toEqual(contact);
                done();
            });
    });

    describe('if there is no contact matching the provided ID', () => {
        it('should return a 400', (done) => {
            request(app)
                .get('/api/contacts/123')
                .expect(400)
                .end(done);
        });

        it('should return a proper error message', (done) => {
            request(app)
                .get('/api/contacts/123')
                .then(({ body }) => {
                    expect(body).toMatchObject({ error: 'Wrong ID' });
                    done();
                });
        });
    });
});

function mockContact() {
    return {
        id: faker.random.uuid(),
        accountId: null,
        name: faker.name.firstName(),
        email: faker.internet.email(),
        addressLine1: faker.address.streetName(),
        addressLine2: faker.address.secondaryAddress(),
        city: faker.address.city(),
        state: faker.address.state(),
        postalCode: faker.address.zipCode(),
        country: faker.address.country(),
    };
};

function mockAccount() {
    return {
        id: faker.random.uuid(),
        companyName: faker.name.firstName(),
        addressLine1: faker.address.streetName(),
        addressLine2: faker.address.secondaryAddress(),
        city: faker.address.city(),
        state: faker.address.state(),
        postalCode: faker.address.zipCode(),
        country: faker.address.country(),
    };
};
