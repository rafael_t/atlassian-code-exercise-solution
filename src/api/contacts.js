import express from 'express';
import pick from 'lodash/pick';

import db from 'db';

const router = express.Router();

const attrs = [
    'accountId',
    'name',
    'email',
    'addressLine1',
    'addressLine2',
    'city',
    'state',
    'postalCode',
    'country',
];

/**
 * Fetch all contacts.
 */
router.get('/', async (req, res) => {
    const contacts = await db.Contact.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
    });
    return res.json(contacts);
});

/**
 * Fetch contact by ID.
 */
router.get('/:id', async (req, res) => {
    const contact = await db.Contact.findByPk(req.params.id, {
        attributes: { exclude: ['createdAt', 'updatedAt'] },
    });
    if (!contact) {
        return res.status(400).json({ error: 'Wrong ID' });
    }
    return res.json(contact);
});

/**
 * Create a contact.
 * Note: Assumes only email is required.
 */
router.post('/', async(req, res) => {
    const contact = pick(req.body, attrs);
    if (!contact.email) {
        return res.status(400).json({ error: 'Missing email' });
    }
    await db.Contact.create(contact);
    return res.sendStatus(201);
});

/**
 * Update a contact.
 * Use this endpoint to associate a contact with an account (via accountId).
 */
router.put('/:id', async (req, res) => {
    const id = req.params.id;
    const contact = await db.Contact.findByPk(id);
    if (!contact) {
        return res.status(400).json({ error: 'Invalid id' });
    }
    const newAccountId = req.body.accountId;
    if (newAccountId) {
        const isValidAccount = !! await db.Account.findByPk(newAccountId);
        if (!isValidAccount) {
            return res.status(400).json({ error: 'Invalid accountId' });
        }
    }
    await contact.update(pick(req.body, attrs));
    return res.sendStatus(200);
});

export default router;
