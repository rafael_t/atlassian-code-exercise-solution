import express from 'express';
import pick from 'lodash/pick';

import db from 'db';

const router = express.Router();

const attrs = [
    'companyName',
    'addressLine1',
    'addressLine2',
    'city',
    'state',
    'postalCode',
    'country',
];

/**
 * Fetch all accounts.
 */
router.get('/', async (req, res) => {
    const accounts = await db.Account.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
    });
    return res.json(accounts);
});

/**
 * Fetch account by ID.
 */
router.get('/:id', async (req, res) => {
    const account = await db.Account.findByPk(req.params.id, {
        attributes: { exclude: ['createdAt', 'updatedAt'] },
    });
    if (!account) {
        return res.status(400).json({ error: 'Wrong ID' });
    }
    return res.json(account);
});

/**
 * Fetch account contacts.
 */
router.get('/:id/contacts', async (req, res) => {
    const account = await db.Account.findByPk(req.params.id);
    if (!account) {
        return res.status(400).json({ error: 'Wrong ID' });
    }
    const contacts = await account.getContacts({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
    });
    return res.json(contacts);
});

/**
 * Create an account.
 * Note: Assumes only companyName is required.
 */
router.post('/', async(req, res) => {
    const account = pick(req.body, attrs);
    if (!account.companyName) {
        return res.status(400).json({ error: 'Missing companyName' });
    }
    await db.Account.create(account);
    return res.sendStatus(201);
});

/**
 * Update an account.
 */
router.put('/:id', async (req, res) => {
    const id = req.params.id;
    const account = await db.Account.findByPk(id);
    if (!account) {
        return res.status(400).json({ error: 'Invalid id' });
    }
    await account.update(pick(req.body, attrs));
    return res.sendStatus(200);
});

export default router;
