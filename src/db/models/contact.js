export default (sequelize, DataTypes) => {
    const Contact = sequelize.define('Contact', {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        addressLine1: DataTypes.STRING,
        addressLine2: DataTypes.STRING,
        city: DataTypes.STRING,
        state: DataTypes.STRING,
        postalCode: DataTypes.STRING,
        country: DataTypes.STRING,
    }, {});
    Contact.associate = (/*models*/) => {
        // associations can be defined here
    };
    return Contact;
};
