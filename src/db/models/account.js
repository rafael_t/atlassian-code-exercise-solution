export default (sequelize, DataTypes) => {
    const Account = sequelize.define('Account', {
        companyName: DataTypes.STRING,
        addressLine1: DataTypes.STRING,
        addressLine2: DataTypes.STRING,
        city: DataTypes.STRING,
        state: DataTypes.STRING,
        postalCode: DataTypes.STRING,
        country: DataTypes.STRING,
    }, {});
    Account.associate = (models) => {
        Account.hasMany(models.Contact, {
            foreignKey: {
                name: 'accountId',
                field: 'accountId',
            },
        });
    };
    return Account;
};
