import config from 'config';

import app from './app';

const {
    host,
    port,
    protocol,
} = config.get('server');

app.listen(
    port,
    () => console.log(`App listening on ${ protocol }://${ host }:${ port }`), // eslint-disable-line no-console
);
