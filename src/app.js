import express from 'express';
import bodyParser from 'body-parser';

import health from './api/health';
import contacts from './api/contacts';
import accounts from './api/accounts';

const app = express();

app.use(bodyParser.json());

app.use('/api/health', health);
app.use('/api/contacts', contacts);
app.use('/api/accounts', accounts);

export default app;
