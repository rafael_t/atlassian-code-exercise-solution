/* eslint-disable import/no-commonjs */
module.exports = {
    cache: false,
    modulePaths: [
        '<rootDir>/src',
    ],
    roots: [
        '<rootDir>/src',
    ],
    testEnvironment: 'node',
    transform: { '.*': '<rootDir>/node_modules/babel-jest' },
    verbose: false,
};
